import xport
import bottle


app = bottle.Bottle()


@app.get('/')
def home():
    string = '''
    <html>
    <body>
    <h1>XPT to CSV file converter</h1>
        <form action="/upload" method="post" enctype="multipart/form-data">
        Select a XPT file: <input type="file" name="upload" />
        <input type="submit" value="Start upload" />
        </form>
    '''
    for fmt in ['json', 'csv']:
        string += '''
    <h1>XPT to {fmt} converter</h1>
        <form action="/upload/{fmt}" method="post" enctype="multipart/form-data">
        Select a XPT file: <input type="file" name="upload" />
        <input type="submit" value="Start upload" />
        </form>
    '''.format(fmt=fmt)
    string += ''' </body> </html> '''
    return string


@app.post('/upload')
def do_upload():
    upload = bottle.request.files.get('upload')
    upload.save('a.xpt', overwrite=True)
    with open('a.xpt', 'rb') as fl:
        df = xport.to_dataframe(fl)
        df.to_csv('a.csv', index=False)
    return bottle.static_file('a.csv', root='.')


@app.post('/upload/json')
def json_upload():
    upload = bottle.request.files.get('upload')
    upload.save('a.xpt', overwrite=True)
    with open('a.xpt', 'rb') as fl:
        df = xport.to_dataframe(fl)
        json = df.to_json(None)
    return json


@app.post('/upload/csv')
def csv_upload():
    upload = bottle.request.files.get('upload')
    upload.save('a.xpt', overwrite=True)
    with open('a.xpt', 'rb') as fl:
        df = xport.to_dataframe(fl)
        out = df.to_csv(None)
    return out

if __name__ == '__main__':
    app.run(debug=True)
